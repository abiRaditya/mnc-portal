import React from "react";
import Schedule from "../../components/Schedule";
import Headline from "../../components/Headline";
import MoreNews from "../../components/MoreNews";
import MobileHeadline from "../../components/MobileHeadline";
import MoreNewsMobile from "../../components/MoreNewsMobile";

const Home = () => {
  return (
    <div>
      <div className="desktop">
        <Schedule />
        <Headline />
        <MoreNews />
      </div>
      <div className="mobile">
        <Schedule />
        <MobileHeadline />
        <MoreNewsMobile />
      </div>
    </div>
  );
};

export default Home;
