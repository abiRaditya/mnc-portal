// import logo from "./logo.svg";
import "./App.scss";
import { Routes, Route } from "react-router-dom";
import Home from "./screens/home/Home";
import StatisHeader from "./components/StatisHeader";
import Header from "./components/Header";
import NavBar from "./components/NavBar";
import Footer from "./components/Footer";
import StatisFooter from "./components/StatisFooter";
import MobileHeader from "./components/MobileHeader";
import "react-multi-carousel/lib/styles.css";
import MobileFoot from "./components/MobileFoot";

function App() {
  return (
    <div className="App">
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}
      <StatisHeader />
      <div className="app-content">
        <Header />
        <NavBar />
        <MobileHeader />
        <Routes>
          <Route path="/" element={<Home />} />
        </Routes>
      </div>
      <Footer />
      <MobileFoot />
      <StatisFooter />
    </div>
  );
}

export default App;
