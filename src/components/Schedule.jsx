import React from "react";
import "react-multi-carousel/lib/styles.css";
import Carousel from "react-multi-carousel";
import ScheduleDetail from "./ScheduleDetail";

const Schedule = () => {
  const scheduleOptions = [
    {
      text: "Jadwal Terbaru",
      value: "8",
    },
    {
      text: "Option 1",
      value: "7",
    },
    {
      text: "Option 2",
      value: "6",
    },
    {
      text: "Option 3",
      value: "5",
    },
  ];
  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 5,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 3,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 3,
    },
  };
  return (
    <div className="schedule flex-content-center container">
      <div className="jadwal-select">
        <div className="title-jadwal-hasil font-robotto fs-20 ">
          JADWAL &amp; HASIL
          <select className="kategori-select">
            {scheduleOptions.map((opt, index) => {
              return (
                <option key={index} value={opt.value}>
                  {opt.text}
                </option>
              );
            })}
          </select>
        </div>
      </div>
      <div className="detail-jadwal">
        <Carousel responsive={responsive}>
          <ScheduleDetail></ScheduleDetail>
          <ScheduleDetail></ScheduleDetail>
          <ScheduleDetail></ScheduleDetail>
          <ScheduleDetail></ScheduleDetail>
          <ScheduleDetail></ScheduleDetail>
          <ScheduleDetail></ScheduleDetail>
        </Carousel>
      </div>
    </div>
  );
};

export default Schedule;
