import React from "react";

const HotTopic = () => {
  return (
    <div className="hot-topics-container">
      <div className="title-section fs-20 font-robotto fw-700 co-black2">
        HOT TOPICS
      </div>
      <div className="hot-topics">
        <ul className="font-robotto fw-700 fs-14">
          <li>
            <i className="fa-solid fa-chevron-right"></i>
            <span>Sepak Bola</span>
          </li>
          <li>
            <i className="fa-solid fa-chevron-right"></i>
            <span>Moto GP</span>
          </li>
          <li>
            <i className="fa-solid fa-chevron-right"></i>
            <span>Bola Basket</span>
          </li>
          <li>
            <i className="fa-solid fa-chevron-right"></i>
            <span>Badminton</span>
          </li>
          <li>
            <i className="fa-solid fa-chevron-right"></i>
            <span>Tenis</span>
          </li>
          <li>
            <i className="fa-solid fa-chevron-right"></i>
            <span>Formula 1</span>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default HotTopic;
