import React from "react";

const MobileHeader = () => {
  return (
    <div className="mobile-header">
      <div className="header-top">
        <div>
          <i className="fa-solid fa-bars"></i>
        </div>
        <div className="sportstar-logo">
          <img
            style={{ width: "198px" }}
            src={require("../assets/sportstars2.png")}
            alt="logo"
          />
        </div>
        <div>
          <i className="fa fa-search" aria-hidden="true"></i>
        </div>
      </div>
      <nav className="quick-nav " id="navbar-mobile">
        <div className="quick-menu font-robotto fs-16 co-white ">
          <div className="quick-item active ">Beranda</div>
          <div className="quick-item">Bola</div>
          <div className="quick-item">Balap</div>
          <div className="quick-item">Ragam</div>
          <div className="quick-item"> Sportaiment</div>
          <div className="quick-item">Hobi</div>
          <div className="quick-item">Data Statistik</div>
          <div className="quick-item">Multimedia</div>
          <div className="quick-item">Index</div>
        </div>
      </nav>
    </div>
  );
};

export default MobileHeader;
