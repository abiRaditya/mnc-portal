import React from "react";

const Header = () => {
  return (
    <div className="header desktop">
      <div className="wrapper">
        <div className="sports-logo">
          <a>
            <img src={require("../assets/sportstars2.png")} alt="logo" />
          </a>
        </div>
        <div className="sports-banner">
          <img
            style={{ width: "100%" }}
            src={require("../assets/header-banner.png")}
            alt=""
          />
        </div>
      </div>
    </div>
  );
};

export default Header;
