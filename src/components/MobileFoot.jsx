import React from "react";

const MobileFoot = () => {
  return (
    <footer className="mobile ">
      <div className="mobile-footer">
        <div className="footer-logo">
          <img
            style={{ width: "100%" }}
            src={require("../assets/sportstars.png")}
            alt="logo"
          />
        </div>
        <div className="footer-menu">
          <ul>
            <li>
              <a>Tentang Kami</a>
            </li>
            <li>
              <a>Redaksi</a>
            </li>
            <li>
              <a>Kode Etik</a>
            </li>
            <li>
              <a>Disclaimer</a>
            </li>
            <li>
              <a>Term Of Service</a>
            </li>
            <li>
              <a>Privacy Policy</a>
            </li>
            <li>
              <a>Sitemap</a>
            </li>
            <li>
              <a>Kontak Kami</a>
            </li>
          </ul>
        </div>
        <div className="footer-links">
          <ul>
            <li>
              <a className="social-footer" target="_blank">
                <i className="fab fa-facebook-f fa-fw"></i>
              </a>
            </li>
            <li>
              <a className="social-footer" target="_blank">
                <i className="fab fa-twitter fa-fw"></i>
              </a>
            </li>
            <li>
              <a className="social-footer" target="_blank">
                <i className="fab fa-instagram fa-fw"></i>
              </a>
            </li>
            <li>
              <a className="social-footer" target="_blank">
                <i className="fab fa-youtube fa-fw"></i>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </footer>
  );
};

export default MobileFoot;
