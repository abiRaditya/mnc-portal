import React from "react";
const Footer = () => {
  return (
    <footer className="d-flex footer-sports desktop">
      <div className="warp-logo-footer">
        <div className="img-logo-footer">
          {/* sportstars.png */}
          <img
            style={{ width: "100%" }}
            src={require("../assets/sportstars.png")}
            alt="logo"
          />
        </div>
        <div className="wrap-social-footer">
          <div className="title-social-footer fs-20 fw-400 font-robotto co-white">
            Connect with us :
          </div>
          <ul>
            <li>
              <a className="social-footer" target="_blank">
                <i className="fab fa-facebook-f fa-fw"></i>
              </a>
            </li>
            <li>
              <a className="social-footer" target="_blank">
                <i className="fab fa-twitter fa-fw"></i>
              </a>
            </li>
            <li>
              <a className="social-footer" target="_blank">
                <i className="fab fa-instagram fa-fw"></i>
              </a>
            </li>
            <li>
              <a className="social-footer" target="_blank">
                <i className="fab fa-youtube fa-fw"></i>
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div className="wrap-about">
        <div className="title-footer font-robotto fw-700 fs-20 co-white">
          ABOUT US
        </div>
        <ul className="font-robotto fs-20 fw-400">
          <li>
            <a>Tentang Kami</a>
          </li>
          <li>
            <a>Disclaimer</a>
          </li>
          <li>
            <a>Kode Etik</a>
          </li>
          <li>
            <a>Term Of Service</a>
          </li>
          <li>
            <a>Privacy Policy</a>
          </li>
          <li>
            <a>Kontak Kami</a>
          </li>
        </ul>
      </div>
      <div className="wrap-kanal">
        <div className="title-footer font-robotto fw-700 fs-20 co-white">
          KANAL UTAMA
        </div>
        <div className="kanal-container d-flex">
          <div className="kanal">
            <ul className="font-robotto fs-20 fw-400">
              <li>
                <a>Beranda</a>
              </li>
              <li>
                <a>Bola</a>
              </li>
              <li>
                <a>Balap</a>
              </li>
              <li>
                <a>Ragam</a>
              </li>
              <li>
                <a>Sportaiment</a>
              </li>
              <li>
                <a>Hobi</a>
              </li>
            </ul>
          </div>
          <div className="kanal">
            <ul className="font-robotto fs-20 fw-400">
              <li>
                <a>Beranda</a>
              </li>
              <li>
                <a>Bola</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
