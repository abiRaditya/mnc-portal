import React from "react";

const BigLatest = () => {
  return (
    <div className="big-latest">
      <img src={require("../assets/news/big-latest.png")} alt="" />
      <div className="news-information">
        <div className="news-header">
          <span className="news-category co-white font-robotto fw-400 fs-15">
            MOTO GP
          </span>
          <span className="news-date co-white font-robotto fw-400 fs-12">
            Rabu, 30 Maret 2020
          </span>
        </div>
        <div className="font-robotto fs-20 f2-700 co-red2">
          <p>GP Sepang 2021</p>
        </div>
        <a className="font-robotto fw-700 fs-15 news-link" href="">
          <h1>Disalip Jelang Finis, Joan Mir Hanya Bisa Tersenyum Getir</h1>
        </a>
      </div>
    </div>
  );
};

export default BigLatest;
