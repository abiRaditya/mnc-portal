import React from "react";

const BigHeadline = () => {
  return (
    <div className="big-headline">
      <img src={require("../assets/news/big-headline.png")} alt="" />
      <div className="news-information">
        <div className="news-header">
          <span className="news-category co-white font-robotto fw-400 fs-15">
            BOLA
          </span>
          <span className="news-date co-white font-robotto fw-400 fs-12">
            Rabu, 30 Maret 2020
          </span>
        </div>
        <a className="font-robotto fw-700 fs-15 news-link" href="">
          <h1>
            Gara-Gara Santiago Bernabeu, EL Real Batal Datangkan Cristiano
            Ronaldo?
          </h1>
        </a>
        <div className="news-desc font-robotto fw-500 fs-16 co-white">
          <p>
            SPANYOL - Presiden Real Madrid, Ramon Calderon, agak ragu mantan
            timnya belanja jorjoran pada bursa transfer musim panas 2021
          </p>
        </div>
      </div>
    </div>
  );
};

export default BigHeadline;
