import React from "react";

const SmallHeadline = ({ style, data }) => {
  //   const image = require(imageString);
  return (
    <div className="small-headline" style={style}>
      <img
        src={
          data ? data.image() : require("../assets/news/small-headline1.png")
        }
        alt=""
      />
      <div className="news-information">
        <div className="news-header">
          <span className="news-category co-white font-robotto fw-400 fs-15">
            {data ? data.category : "SPORTAIMENT"}
          </span>
          {data ? (
            ""
          ) : (
            <span className="news-date co-white font-robotto fw-400 fs-12">
              Rabu, 30 Maret 2020
            </span>
          )}
        </div>
        <a className="font-robotto fw-700 fs-15 news-link" href="">
          <h2>
            {data
              ? data.title
              : `Bambang Pamungkas, Legenda Sepanjang Masa Persija dan Timnas
            Indonesia`}
          </h2>
        </a>
      </div>
    </div>
  );
};

export default SmallHeadline;
