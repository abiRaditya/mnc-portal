import React from "react";

const StatisFooter = () => {
  return (
    <div className="copyright">
      © Copyrights 2021. Sportstars.id All rights reserved
    </div>
  );
};

export default StatisFooter;
