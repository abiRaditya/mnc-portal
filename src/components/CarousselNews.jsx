import React from "react";

const CarousselNews = () => {
  return (
    <div className="caroussel-news">
      <img
        src={require("../assets/news/caroussel1.png")}
        alt=""
        draggable={false}
      />
      <div className="news-information">
        <div className="font-robotto fs-16 fw-700 co-white">
          IBL 2021: West Bandits Berhasil Balas Kekalahan atas NSH
        </div>
        <div className="lastest-date d-flex">
          <img src={require("../assets/calender.svg").default} alt="" />
          <p
            className="co-white fs-12 font-robotto fw-500"
            style={{ margin: "unset" }}
          >
            31 Maret 2021
          </p>
        </div>
      </div>
    </div>
  );
};

export default CarousselNews;
