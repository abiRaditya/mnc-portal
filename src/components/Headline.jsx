import React from "react";
import BigHeadline from "./BigHeadline";
import SmallHeadline from "./SmallHeadline";

const Headline = () => {
  return (
    <div className="headline">
      <div className="container headline-desktop">
        <div className="headline-top d-flex">
          <BigHeadline />
          <div className="small-container ">
            <SmallHeadline style={{ marginBottom: "8px" }} />
            <SmallHeadline />
          </div>
        </div>
        <div className="headline-bottom d-flex" style={{ marginTop: "8px" }}>
          <SmallHeadline />
          <SmallHeadline />
          <SmallHeadline />
        </div>
      </div>
    </div>
  );
};

export default Headline;
