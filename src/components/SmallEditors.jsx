import React from "react";

const SmallEditors = () => {
  return (
    <div className="small-editors d-flex">
      <img src={require("../assets/small-editor.png")} alt="" />
      <div className="small-info">
        <h2 className="font-robotto fw-50 fs-16 co-black2">
          Hasil Pertandingan NBA 2020-2021{" "}
        </h2>
        <div className="lastest-date d-flex">
          <img src={require("../assets/calender.svg").default} alt="" />
          31 Maret 2021
        </div>
      </div>
    </div>
  );
};

export default SmallEditors;
