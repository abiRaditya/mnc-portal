import React, { useState, useEffect } from "react";

const NavBar = () => {
  const [scroll, setScroll] = useState(false);
  useEffect(() => {
    window.addEventListener("scroll", () => {
      setScroll(window.scrollY > 100);
    });
  }, []);
  return (
    <nav id="navbar" className={scroll ? "sticky desktop" : "desktop"}>
      <div className="container">
        <ul className="font-robotto fs-16 fw-500 co-white">
          <li className="active ">
            <a>BERANDA</a>
          </li>
          <li>
            <a>BOLA</a>
          </li>
          <li>
            <a>BALAP</a>
          </li>
          <li>
            <a>RAGAM</a>
          </li>
          <li>
            <a>SPORTAIMENT</a>
          </li>
          <li>
            <a>HOBI</a>
          </li>
          <li>
            <a>DATA STATISTIK</a>
          </li>
          <li>
            <a>MULTIMEDIA</a>
          </li>
          <li>
            <a>INDEX</a>
          </li>
        </ul>
        <div className="search-button">
          <i className="fa fa-search" aria-hidden="true"></i>
        </div>
      </div>
    </nav>
  );
};

export default NavBar;
