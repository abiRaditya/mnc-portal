import React from "react";
import Carousel from "react-multi-carousel";
import SmallHeadline from "./SmallHeadline";
const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 1,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 1,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 1,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
};
const headlineStyle = {
  //   width: "355px",
  //   height: "240px",
};
const MobileHeadline = () => {
  return (
    <div className="headline">
      <Carousel responsive={responsive}>
        <SmallHeadline style={headlineStyle} />
        <SmallHeadline style={headlineStyle} />
        <SmallHeadline style={headlineStyle} />
        <SmallHeadline style={headlineStyle} />
        <SmallHeadline style={headlineStyle} />
        <SmallHeadline style={headlineStyle} />
      </Carousel>
    </div>
  );
};

export default MobileHeadline;
