import React from "react";
import EachNewsMobile from "./EachNewsMobile";
import PopularNews from "./PopularNews";
import Carousel from "react-multi-carousel";
import LatestMobile from "./LatestMobile";
import Multimedia from "./Multimedia";
import SmallHeadline from "./SmallHeadline";
import SmallEditors from "./SmallEditors";
import HotTopic from "./HotTopic";
import StatistikTable from "./StatistikTable";
import StatistikControl from "./StatistikControl";

const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 5,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 2,
  },
};

const MoreNewsMobile = () => {
  return (
    <>
      <div className="banner-sports">
        <img
          style={{ width: "100%" }}
          src={require("../assets/mobileBanner.png")}
          alt=""
        />
      </div>
      <EachNewsMobile />
      <div className="more-news-mobile">
        <div className="title-section fs-20 font-robotto fw-700">
          LATEST NEWS
        </div>
        <LatestMobile />
        <div className="title-section fs-20 font-robotto fw-700">
          MOST POPULAR
        </div>
        <div className="most-popular-mobile">
          <Carousel responsive={responsive}>
            <PopularNews />
            <PopularNews />
            <PopularNews />
            <PopularNews />
            <PopularNews />
          </Carousel>
        </div>
        <LatestMobile />
        <LatestMobile />
      </div>
      <Multimedia />
      <div className="more-news-mobile">
        <div className="title-section fs-20 font-robotto fw-700">
          EDITOR'S CHOICE
        </div>
        <div className="edtors-choice-mobile">
          <SmallHeadline />
          <SmallEditors />
          <SmallEditors />
          <SmallEditors />
          <SmallEditors />
        </div>
        <HotTopic />
        <div className="title-section fs-20 font-robotto fw-700">
          STATISTIK <span>Lihat Detail</span>
        </div>
        <StatistikControl />
        <StatistikTable />
      </div>
    </>
  );
};

export default MoreNewsMobile;
