import React from "react";
const options = [
  {
    text: "EURO 2021",
    value: "8",
  },
  {
    text: "Option 1",
    value: "7",
  },
  {
    text: "Option 2",
    value: "6",
  },
  {
    text: "Option 3",
    value: "5",
  },
];
const SelectStatistik = () => {
  return (
    <div className="select-statistik">
      <select className="kategori-select">
        {options.map((opt, index) => {
          return (
            <option key={index} value={opt.value}>
              {opt.text}
            </option>
          );
        })}
      </select>
    </div>
  );
};

export default SelectStatistik;
