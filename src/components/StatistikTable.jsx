import React from "react";

const StatistikTable = () => {
  return (
    <>
      <div className="statistik-table">
        <table>
          <thead>
            <tr>
              <th>Rank</th>
              <th>P</th>
              <th>GD</th>
              <th>Pts</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <div className="statistik-data">
                  <div className="flag-number">
                    <span>1</span>
                    <img src={require("../assets/Turkey.png")} alt="" />
                  </div>
                  <p>Turkey</p>
                </div>
              </td>

              <td>6</td>
              <td>8</td>
              <td>13</td>
            </tr>
            <tr>
              <td>
                <div className="statistik-data">
                  <div className="flag-number">
                    <span>1</span>
                    <img src={require("../assets/Turkey.png")} alt="" />
                  </div>
                  <p>Turkey</p>
                </div>
              </td>

              <td>6</td>
              <td>8</td>
              <td>13</td>
            </tr>
            <tr>
              <td>
                <div className="statistik-data">
                  <div className="flag-number">
                    <span>1</span>
                    <img src={require("../assets/Turkey.png")} alt="" />
                  </div>
                  <p>Turkey</p>
                </div>
              </td>

              <td>6</td>
              <td>8</td>
              <td>13</td>
            </tr>
            <tr>
              <td>
                <div className="statistik-data">
                  <div className="flag-number">
                    <span>1</span>
                    <img src={require("../assets/Turkey.png")} alt="" />
                  </div>
                  <p>Turkey</p>
                </div>
              </td>

              <td>6</td>
              <td>8</td>
              <td>13</td>
            </tr>
          </tbody>
        </table>
      </div>
    </>
  );
};

export default StatistikTable;
