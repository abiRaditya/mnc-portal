import React from "react";
import Carousel from "react-multi-carousel";
import EachNews from "./EachNews";
const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 5,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 2,
  },
};
const EachNewsMobile = () => {
  return (
    <Carousel responsive={responsive}>
      <EachNews />
      <EachNews />
      <EachNews />
      <EachNews />
      <EachNews />
      <EachNews />
    </Carousel>
  );
};

export default EachNewsMobile;
