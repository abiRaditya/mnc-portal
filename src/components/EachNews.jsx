import React from "react";

const EachNews = () => {
  return (
    <div className="each-news">
      <img
        style={{ width: "100%" }}
        src={require("../assets/news/each-news1.png")}
        alt=""
      />
      <div className="news-information">
        <a className="font-robotto fw-700 fs-14 news-link" href="">
          Anthony Ginting Ungkap Sisi Positif Indonesia Badminton
        </a>
      </div>
    </div>
  );
};

export default EachNews;
