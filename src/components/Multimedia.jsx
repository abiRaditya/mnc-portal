import React from "react";
import CarousselNews from "./CarousselNews";
import Carousel from "react-multi-carousel";
const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 5,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 2,
  },
};

const Multimedia = () => {
  return (
    <div className="multimedia-container">
      <div className="title-section fs-20 font-robotto fw-700 co-white">
        MULTIMEDIA
      </div>
      <div className="multimedia-items">
        <Carousel className="caroussel" responsive={responsive}>
          <CarousselNews></CarousselNews>
          <CarousselNews></CarousselNews>
          <CarousselNews></CarousselNews>
          <CarousselNews></CarousselNews>
          <CarousselNews></CarousselNews>
        </Carousel>
      </div>
    </div>
  );
};

export default Multimedia;
