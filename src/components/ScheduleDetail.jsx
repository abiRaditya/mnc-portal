import React from "react";

const ScheduleDetail = () => {
  return (
    <div draggable={false} className="detail-jadwal-item">
      <div draggable={false} className="tgl-jadwal-hasil">
        <div className="title-jadwal font-robott co-black fw-500">
          Liga Inggris Premier League
        </div>
        <div className="date-jadwal font-robotto fw-500 fs-12 co-bright-red">
          Rabu, 31 Maret 2021
        </div>
      </div>
      <div draggable={false} className="tim-jadwal font-robotto d-flex">
        <div className="tim-flag">
          <img
            draggable={false}
            src={require("../assets/germany.png")}
            alt=""
          />
          <span className="tim-name font-robotto fw-500 fs-16">BEL</span>
        </div>
        <div className="tim-result font-robotto fw-500 fs-16">2</div>
      </div>
      <div draggable={false} className="tim-jadwal font-robotto d-flex">
        <div className="tim-flag">
          <img draggable={false} src={require("../assets/france.png")} alt="" />
          <span className="tim-name font-robotto fw-500 fs-16">FRA</span>
        </div>
        <div className="tim-result font-robotto fw-500 fs-16">0</div>
      </div>
      <div
        draggable={false}
        className="pre-match font-robotto fw-700 fs-14 co-blue2"
      >
        Pre Match
      </div>
    </div>
  );
};

export default ScheduleDetail;
