import React from "react";
import EachNews from "./EachNews";
import LatestNews from "./LatestNews";
import BigLatest from "./BigLatest";
import PopularNews from "./PopularNews";

import SmallHeadline from "./SmallHeadline";
import SmallEditors from "./SmallEditors";
import Multimedia from "./Multimedia";
import HotTopic from "./HotTopic";
import StatistikTable from "./StatistikTable";
import SelectStatistik from "./SelectStatistik";

const MoreNews = () => {
  return (
    <div className="more-news container">
      <div className="left-side">
        <div className="banner-sports">
          <img
            style={{ width: "100%" }}
            src={require("../assets/banner.png")}
            alt=""
          />
        </div>

        <div className="news-container">
          <EachNews />
          <EachNews />
          <EachNews />
          <EachNews />
        </div>

        <div className="latest-news-container">
          <div className="title-section fs-20 font-robotto fw-700">
            LATEST NEWS
          </div>
          <LatestNews />
        </div>

        <div className="big-latest-container">
          <BigLatest />
        </div>

        <div className="most-popular-container">
          <div
            className="title-section fs-20 font-robotto fw-700"
            style={{ marginBottom: "15px" }}
          >
            MOST POPULAR
          </div>
          <div className="d-flex">
            <PopularNews />
            <PopularNews />
            <PopularNews />
            <PopularNews />
          </div>
        </div>

        <div className="list-latest-news-container">
          <LatestNews />
          <LatestNews />
        </div>

        <Multimedia />
      </div>
      <div className="right-side">
        <div className="title-section fs-20 font-robotto fw-700 co-black2">
          EDITOR'S CHOICE
        </div>
        <SmallHeadline
          style={{
            width: "300px",
            height: "250px",
            fontSize: "18px",
            fontWeight: "500",
            marginTop: "15px",
            marginBottom: "15px",
            borderBottom: "1px solid #f2f2f2 ",
          }}
          data={{
            image: () => {
              return require("../assets/news/editors-choice.png");
            },
            date: null,
            title:
              "Bangga, Zarco Bongkar Rahasianya Pecahkan Rekor Top Speed MotoGP",
            category: "Moto GP",
          }}
        />
        <div className="more-editors">
          <SmallEditors></SmallEditors>
          <SmallEditors></SmallEditors>
          <SmallEditors></SmallEditors>
          <SmallEditors></SmallEditors>
        </div>

        <HotTopic />
        <div className="title-section fs-20 font-robotto fw-700 co-black2">
          STATISTIK{" "}
          <span>
            <SelectStatistik />
          </span>
        </div>
        <StatistikTable />
        <StatistikTable />
      </div>
    </div>
  );
};

export default MoreNews;
