import React from "react";
import SelectStatistik from "./SelectStatistik";

const StatistikControl = () => {
  return (
    <div className="statistik-control">
      <SelectStatistik />
      <div className="control-button">
        <i class="fa-solid fa-circle-chevron-left"></i>
        <i class="fa-solid fa-circle-chevron-right"></i>
      </div>
    </div>
  );
};

export default StatistikControl;
