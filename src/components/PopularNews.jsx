import React from "react";

const PopularNews = () => {
  return (
    <div className="most-popular-news">
      <img
        className="popular-image"
        src={require("../assets/news/popular1.png")}
        alt=""
      />
      <div className=" d-flex" style={{ alignItems: "flex-end" }}>
        <img src={require("../assets/calender.svg").default} alt="" />
        31 Maret 2021
      </div>
      <a href="" className="font-robotto fw-500 fs-16 news-link co-black2">
        <h3>Jadwal MotoGP Doha 2021: Valentino Rossi Memble Lagi?</h3>
      </a>
    </div>
  );
};

export default PopularNews;
