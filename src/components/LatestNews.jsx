import React from "react";

const LatestNews = () => {
  return (
    <article className="latest-news d-flex">
      <div className="latest-image">
        <img src={require("../assets/news/lastest-news.png")} alt="" />
      </div>
      <div className="lastest-desc">
        <div className="lastest-category">
          <p className="font-robotto fs-16 fw-500">F1</p>
          <div className="lastest-date d-flex">
            <img src={require("../assets/calender.svg").default} alt="" />
            31 Maret 2021
          </div>
        </div>

        <div className="font-robotto fw-700 fs-14 co-red2 ">
          <p>Kualifikasi Piala Dunia 2022</p>
        </div>
        <div className="font-robotto fs-20 fw-700 co-black2">
          <p>Hamilton Menangi Persaingan Sengit atas Verstappen</p>
        </div>
        <div className="font-robotto fs-15 fw-400 co-black2">
          <p>
            Pembalap Tim Mercedes, Lewis Hamilton, tampil sebagai pemenang di
            ajang Formula One (F1) GP Bahrain 2021. Akan tetapi, kemenangan ini
            tak didapat Hamilton dengan mudah karena ia harus bersaing sengit
            dengan pembalap Red Bull, Max Verstappen.
          </p>
        </div>
      </div>
    </article>
  );
};

export default LatestNews;
