import React from "react";

const StatisHeader = () => {
  return (
    <div className="statis-header desktop">
      <div className="links">
        <a className="link-font fw-600 fs-14">RCTI+</a>
        <a className="link-font fw-600 fs-14">Vision+</a>
        <a className="link-font fw-600 fs-14">Okezone.com</a>
        <a className="link-font fw-600 fs-14">SINDOnews.com</a>
        <a className="link-font fw-600 fs-14">iNews.id</a>
        <a className="link-font fw-600 fs-14">Buddyku</a>
        <i className="fa fa-chevron-down"></i>
      </div>
      <div className="social-media links">
        <a href="">
          <img alt="" src={require("../assets/Facebook.svg").default} />
        </a>
        <a href="">
          <img alt="" src={require("../assets/Twitter.svg").default} />
        </a>
        <a href="">
          <img alt="" src={require("../assets/Instagram.svg").default} />
        </a>
        <a href="">
          <img alt="" src={require("../assets/Youtube.svg").default} />
        </a>
        <a href="">
          <img alt="" src={require("../assets/TikTok.svg").default} />
        </a>
      </div>
    </div>
  );
};

export default StatisHeader;
