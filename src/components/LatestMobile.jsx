import React from "react";

const LatestMobile = () => {
  return (
    <div className="latest-mobile">
      <div className="latest-image">
        <img src={require("../assets/news/lastest-news.png")} alt="" />
      </div>
      <p className="font-robotto fs-16 fw-500 co-red2">Moto GP</p>
      <div className="lastest-date d-flex">
        <img src={require("../assets/calender.svg").default} alt="" />
        31 Maret 2021
      </div>
      <div className="font-robotto fs-18 fw-700 co-black2">
        <p>
          Rossi dan Morbidelli Beda Pandangan Soal Balapan di MotoGP Doha, 4
          April
        </p>
      </div>
    </div>
  );
};

export default LatestMobile;
